# Reference video sessions


#### Go best practices


**GopherCon EU 2018: Peter Bourgon - Best Practices for Industrial Programming**

https://www.youtube.com/watch?v=PTE4VJIdHPg

**Notes**

Go's type system is structural; not Nominal: Interfaces are behavioral contracts that general belong in consuming code
* Accept interfaces and return structs


Define interfaces at the major fault lines in your architecture
* Between func main and the rest
* Along package API boundaries

Interfaces Reify abstraction boundaries
* Use them to help structure your thoughts and desing (especially when writing tests)

Using context for lifecycle management is generally a good idea and infectious.


#### Go testing

** Mitchel Hashimoto - Advanced testing with go**

https://www.youtube.com/watch?v=8hQG7QlcLBk

https://www.youtube.com/watch?v=yszygk1cpEc


