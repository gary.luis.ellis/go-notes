# Interfaces notes

### What is an interface
An interface is a shared boundary across which two or more separate components of a computer system exchange information. In OOP languages the term interface is used to define an abstract type that contains no data but defines behaviors as method signatures. This applies in go. In go, an interface is an abstract type.

### Abstract types in go
Abstract types in go
* Describe behavior
* Define a set of methods, without specifying the receiver
* A receiver implements the interface
*they define an area in the space of concrete types

### why do we use interfaces
* writing generic algorithms
* hiding implementation details
* providing interception points


### Interface design principles
The smaller the interface you choose, the more reusable your function will be

The bigger the interface, the weaker the abstraction



##### The Robustness Principle applied to golang
Be conservative in what you do, be liberal in what you accept from others - (used to model the tcp protocol)
Robustness Principle applied to go - *"Return concret types, receive interfaces as parameters"* This only a general rule of thumb as it isn't neccessarily applicible to to hiding implementation details.



#### Interfaces hide implementation details
Hiding implementation details with interface does the following:
- decouples implementation from API
- allows to easily switch between implementations and or provide multiple ones


#### Interfaces as interception points.
`f.Do()` 
First we need to understand a little bit about concrete types and abstract types to understand that interfaces are powerful for dynamic dispatch of calls during runtime.

Concrete types: static
- known at compilation
- very efficient
- can't intercept

Abstract types: dynamic
- unknown at compilation
- less efficient
- easy to intercept

Interfaces: dynamic dispatch of calls
```go
// http.Transport has one interface Transport. transport is not known at compile time.
// i.e. https, http, proxy, etc. it needs to be dynamic at runtime.
type Client struc t{
    Transport RoundTripper
    ...
}

type RoundTripper interface {
    RoundTrip(*Request) (*Response, error)
}
```

```go
type headers Struct {
    rt http.RoundTripper
    v map[string]string
}

func (h headers) RoundTrip(r *http.Request) *http.Response {
    for k, v := range h.v {
        r.Header.Set(k, v)
    }
    return h.rt.RoundTrip(r)
}

// following demonstrates intercepting. 
c := &http.Client{
    Transport: headerRoundTripper{
        rt: http.DefaultTransport,
        v: map[string]string{"foo": "bar"},
    },
}

res, err := c.Get("http://golang.org")
```


#### Interfaces in go specfiically
* are implimented implicitly.
* can break dependencies
Define interfaces where you use them.


#### interfaces can be used for type assertions
They can be used to perform type assertions to identify concrete type
```go
func do(v interface{} ) {
    i := v.(int) // panics if type is not int

    i, ok := v.(int) // will return false if type is not int
}
```
Another type assertion example
```go
func do(v interface{}) {
    select v.(type) {
    
    case int:
        fmt.Println("got int %d", v)
    default:
        fmt.Println("not sure what type")
    }
}
```

Avoid abstract to concrete assertions because effectively make your code less easy to evolve.
Instead type assert to an interface. i.e. an interface to an interface.
```go
// runtime checks interface to concrete type
func do(v interface{}) {
    select s:- v.(type) {

    case fmt.Stringer
        fmt.Println(s.String())  // s is of type int
    default:
        fmt.Println("not sure what type")
    }
}
```

Type assertions as extension mechanism by many packages in go std library
- fmt.Stringer
- json.Marshaler/Unmarshaler
- ...

they use this pattern to adapt their behavior accordingly.

Use type assertions to extend behavior.
Use type assertions to classify errors (or other types)

Type assertions can be used as an evolution mechanism
* Adding methods to an interface breaks backwards compatibility
* Use type assertions to maintain compatibility

#### type assertions as evolution mechanism
Step 1: add the method to your concrete type implementation

Step 2: define an interface containing the new method

Step 3: document it




### Abstract data types
Mathematical model for data types

Defined by its behavior in terms of:
- possible values,
- possible operations on data of this type,
- and the behavior of these operations


## References
* https://notes.shichao.io/gopl/ch7/
* https://speakerdeck.com/campoy/understanding-the-interface
* https://www.youtube.com/watch?v=F4wUrj6pmSI

