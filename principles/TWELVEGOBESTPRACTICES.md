# Twelve Go Best Practices - Francesc Campoy

Techniques to write healthy Go codebase:
* simple
* Readable
* maintainable


## early return
* Avoid nesting by handling errors first
* Else is not used often in go.

## Avoid repetition when possible
* Deploy one-off utility types for simpler code


## type switch to handle special cases
* Type assertion case statement
* Type switch with short variable declaration


## Writing everything or nothing

## Use function adapters to write less code
* Same as decorators in python



## Make your packages "go get"-able

## Ask for what you need in your API
* Use smallest interface that you can use.

## Keep independent packages independent
* Avoid dependency by using an interface

Example 
```go
import (
    "image"
    "code.google.com/go.talks.2013/bestpractices/funcdraw/parser"
)

// Draw draws an image showing a rendering of the passed ParsedFunc.
func DrawParsedFunc(f parser.ParsedFunc) image.Image {
```
Avoid dependency by using an interface.
```go
import "image"

// Function represent a drawable mathematical function.
type Function Interface {
    Eval(float64) float64
}

// Draw draws an image showing a rendering of the passed Function.
func Draw(f Function) image.Image {
```

## Testing
* Using an interface instead of a concrete type makes testing easier.
```go
package drawer

import (
    "math"
    "testing"
)

type TestFunc func(float64) float64

func (f TestFunc) Eval(x float64) float64 { return f(x)}

var (
    ident = TestFunc(func(x float64) float64 { return x})
    sin = TestFunc(math.Sin)
)

func TestDraw_Ident(t *testing.T) {
    m := Draw(ident)
    // Verify obtained image.
}
```

## Avoid concurrency in your API
```go
func doConcurrently(job string, err chan error) {
    go func() {
        fmt.Println("doing job", job)
        time.Sleep(1 * time.Second)
        err <- errors.New("something went wrong!")
    }()
}

func main() {
    jobs := []string({"one", "two", "three"})

    errc := make(chan error)
    for _, job := range jobs {
        doConcurrently(job, errc)
    }
    for _ := range jobs {
        if err := <-errc; err != nil {
            fmt.Println(err)
        }
    }
}
```
What if we want to use it sequentually?
```go
func do(job string) error {
    fmt.Println("doing job", job)
    time.Sleep(1 * time.Second)
    return errors.New("Something went wrong!")
}

func main() {
    jobs := []string{"one", "two", "three"}

    errc := make(chan error)
    for _, job := range jobs {
        go func(job string){
            errc <- do(job)
        }(job)
    }
    for _ = range jobs {
        if err := <-errc; err != nil {
            fmt.Println(err)
        }
    }
}
```
Expose synchronous APIs, calling them concurrently is easy.

## Use goroutines to manage state
Use a chan or struct with a chan to communicate with a goroutine
```go
type Server struct { quit chan bool}

func NewServer() *Server {
    s := &Server{make(chan bool)}
    go s.Run()
    return s
}

func (s *Server) run() {
    for {
        select {
        case <- s.quit:
            fmt.Println("finishing task")
            time.Sleep(time.Second)
            fmt.Println("task done")
            s.quit <- true
            return
        case <-time.After(time.Second):
          fmt.Println("running task")
        }
    }
}

func (s *Server) Stop() {
    fmt.Println("server stopping")
    s.quit <- true
    <-s.quit
    fmt.Println("server stopped")
}

func main() {
    s := NewServer()
    time.Sleep(2 * time.Second)
    s.Stop()
}
```

## Avoid groutine leaks with buffered chans
```go
func semdMsg(msg, addr string) error {
    conn, err := net.Dial("tcp", addr)
    if err != nil {
        return err
    }
    defer conn.Close()
    _, err = fmt.Fprint(conn, msg)
    return err
}
```
```go
func broadcastMsg(msg string addrs []string) error {
    errc := make(chan error)
    for _, addr := range addrs {
        go func(addr string) {
            errc <- sendMsg(msg, addr)
            fmt.Println("done")
        }(addr)
    }

    for _ = range addrs {
        if err := <-errc; err != nil {
            return err
        }
    }
    return nil
}

func main() {
    addr := []string{"localhost:8080", "http://google.com"}
    err := broadcastMsg("hi", addr)

    time.Sleep(time.Second)

    if err != nil {
        fmt.Println(err)
        return
    }
    fmt.Println("everything went fine")
}
```
* The goroutine is blocked on the chan write
* The goroutine holds a reference to the chan
* the chan will never be garbage collected


##### Solution - avoid groutine leaks with buffered channels
```go
func broadcastMsg(msg string. addrs []string) error {
    errc := make(chan error, len(addrs))
    for _, addr := range addrs {
        go func(addr string) {
            errc <-sendMsg()
            fmt.Println("done")
        }(addr)
    }

    for _ = range addrs {
        if err := <-errc; err != nil {
            return err
        }
    }
    return nil
}
```
* What if we can't predict the capacity of the channel?

##### Solution - avoid goroutine leaks with quit chan
```go
func broadcastMsg(msg string, addrs []string) error {
    errc := make(chan error)
    quit := make(chan struct{})

    defer close(quit)

    for _, addr := range addrs {
        go func(addr string) {
            select {
                case errc <- sendMsg(msg, addr):
                    fmt.Println("done")
                case <-quit:
                    fmt.Println("quit")
            }
        }(addr)
    }

    for  _ = range addrs {
        if err := <-errc; err != nil {
            return err
        }
    }
    return nil
}
```

## Summary of 12 best practices
1. Avoid nesting by handling errors first
2. Avoid repetition when possible
3. Important code goes first
4. Document your code
5. Shorter is better
6. Packages with multiple files
7. Make your packages "go get"-able
8. Ask for what you need
9. Keep independent packages independent
10. Avoid concurrency in your API
11. Use goroutines to manage state
12. Avoid goroutine leaks


# References
https://talks.golang.org/2013/bestpractices.slide#1