# phases and idioms
A great rule of thumb for go is accept interfaces, return structs
- Jack lindamood

Design is the art of arranging code that needs to work **today**, and to be easy to change **forever**.
- Sandi Metz


# SOLID for go
Interfaces let you apply the SOLID principles to Go programs.

## Single Responsibility Principle
Structure your functions and types into packages that exhibit natural cohesion.


## Open Closed Principle
Compose types with embeddign rather than extend them through inheritance.

## Liskov Substitution Principle
Express the dependencies between your packages in terms of interfaces, not concrete types

## Interface Substitution Principle
Define functions and methods that depend only on the behavior that they need. 



## Dependency Inversion Principle
Refactor dependencies from compile time to run time.


high level modules should not depend on low-level modules. Both should depend on abstractions.

Abstractions should not depend on details. Details should depend on abstractions.
- Robert C. Martin

* structure of import graph should wide and fat rather than tall and narrow.


## GO should stop
- go programmers need to talk less about fragments and more about design.
- stop focusing on performance and focus on reuse

## GO should
- talk about how to design go programs that are well engineered, decoupled, reusable, and responsive to changing requirements.

