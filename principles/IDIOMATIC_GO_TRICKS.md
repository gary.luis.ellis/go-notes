# Idiomatic Go Tricks - Golang UK Conference 2016 - Matt Ryer

## Line of sight
allows reader to quickly view what is happening in a program.
* Make return the last statement if possible
* try to avoid else statements and invert logic to if not something. i.e. `if !Something.OK {}`


## Single method interfaces
```go
type Reader interface {
    Read(p []byte) (n int, err error)
}
```
* Interface consisting of only one method
* Simpler = more powerful and useful
* Easy to implement
* Used throughout the standard librry
```go
type Handler interface {
    ServeHttp(ResponseWriter, *Request)
}
```
* Only need to implement one method on a struct to build a handler.


## Function type alternatives for single method interfaces
http.Handler has a counterpart called http.HandlerFunc

```go
// The HandlerFunc type is an adapter to allow the use of
// ordinary functions as HTTP handlers. If f is a function
// with the appropriate signature, HandlerFunc(f) is a
// Handler that calls f.
type HandlerFunc func(ResponseWriter, *Request)

// ServeHTTP calls f(w, r).
func (f HandlerFunc) ServeHTTP(w ResponseWriter, r *Request) {
    f(w, r)
}
```

* func type with matching signature
* Method on that func implementing the interface
* Method just calls the func
* Now you don't even need a struct, you can just use a function.
* This is a commonly used pattern. (I think its called function option pattern?)

## Log blocks
```go
func foo() error {
    log.Println("----------")
    defer log.Println("----------")

    //...
}
```
* Sits at top of the function
* Easy to comment out or remove


## Return teardown functions
```go
func setup(t *testing.T) (*os.File, func(), error) {
    teardown := func() {}
    // make a test file
    f, err := ioutil.TempFile(os.TempDir(), "test")
    if err != nil {
        return nil, teardown, err
    }
    teardown func() {
        // close f
        err := f.Close()
        if err != nil {
            t.Error("setup: Close:", err)
        }
        // delete the test file
        err = os.RemoveAll(f.Name())
        if err != nil {
            t.Error("setup: RemoveAll:", err)
        }
    }
    return f, teardown, nil
}

func TestSomething(t *testing.T) {
    f, teardown, err := setup()
    defer teardown()
    if err != nil {
        t.Error("setup:", err)
    }
    // do something with f
}
```
* Clean-up code is encapsulated
* Caller doesn't need to worry about cleaning up
* If setup changes, code that uses it doesn't necessarily need to


## Good Timing
```go
func StartTimer(name string) func() {
    t := time.Now()
    log.Println(name, "started")
    return func() {
        d := time.Now().Sub(t)
        log.Println(name, "took", d)
    }
}

func FunkyFunc() {
    stop := StartTimer("FunkyFunc")
    defer stop()

    time.Sleep(1 * time.Second)
}
```

```
10:00:01 FunkyFunc started
10:00:01 FunkyFunc took 1.0014159265s
```
* Capture the state in the closure
* Make things easy for your users


## Discover interfaces
```go
// Sizer describes the Size() method that gets the
// total size of an item.
type Sizer interface {
    Size() int64
}

func Fits(capacity int64, v Sizer) bool {
    return capacity > v.Size()
}

func IsEmailable(v Sizer) bool {
    return 1<<20 > v.Size()
}

// Size gets the size of a File.
func (f *fFile) Size() int64 {
    return f.info.Size()
}
```

### Many items as one
Add a new type which is a slice of the interface
```go

// this puts method implementation the slice of Size interface
type Sizers []Sizer

func (s Sizers) Size() int64 {
    var total int64
    for _, sizer := range s {
        total += sizer.Size()
    }
    return total
}
```
* The slice type implements the Sizer interface
* Now a set of objects can be used in Fits and IsEmailable functions


### Other ways to implmenet the interface
```go
type SizeFunc func() int64

func (s SizeFunc) Size() int64 {
    return s()
}

type Size int64

func (s Size) Size() int64 {
    return int64(s)
}
```
* SizeFunc means we can write ad-hoc size calculators and still make use of the same methods.
* Size int64 type means we can specify explicit sizes: Size(123)
* Easy because the interface is so small


### Optional features
```go
type Valid interface {
    OK() error
}
func (p Person) OK() error {
    if p.Name == "" {
        return errors.New("name required")
    }
    return nil
}
func Decoder(r io.Reader, v interface{}) error {
    err := json.NewDecoder(r).Decode(v)
    if err != nil {
        return err
    }
    // call the method on the interface
    obj, ok := v.(Valid)
    if !ok {
        return nil // no OK method
    }
    err = obj.OK()
    if err != nil {
        return err
    }
    return nil
}
```

## Simple mocks
```go
type MailSender interface {
    Send(to, subject, body string) error
    SendFrom(from, to, subject, body string) error
}

type MockSender struct {
    SendFunc      func(to, subject, body, string) error
    SendFromFunc  func(from, to, subject, body string) error
}

func(m MockSender) Send(to, subject, body string) error {
    return m.SendFunc(to, subject, body)
}

func (m MockSender) SendFrom(from, to, subject, body string) error {
    return m.SendFromFunc(from, to, subject, body)
}
```
* Implement the interface but pass execution directly to the function fields.

### using simple mocks example
```go
func TestWelcomeEmail(t *testing.T) {
    errTest := errors.New("nope")
    var msg string

    sender := MockSender{
        SendFunc: func(to, subject, body string) error {
            msg fmt.Sprintf("(%s) %s: %s", to, subject body)
            return nil
        },
        SendFromFunc: func(from, to, subject, body string) error {
            return errTest
        },
    }

    SendWelcomeEmail(sender, "to", "subject", "body")

    if msg != "(to) subject: body" {
        t.Error("SendWelcomeEmail:", msg)
    }
}
```

### Mocking other peoples structs
Sometimes somebody else provides the struct (and not an interface)
```go
package them

type Messenger struct {}
func (m *Messenger) Send(to, message string) error {
    // ... their code
}
```
We want to mock this in test code but there's no interface.

Make your own
```go
type Messenger interface {
    Send(to, message string) error
}
```
* This interface is already implemented by them.Messenger struct
* We are free to mock it in test code or even provide our own implementations


## Retrying
```go
type Func func(attempt int) (retry bool, err error)

func Try(fn Func) error {
    var err error
    var cont bool
    attempt := 1
    for {
        cont, err fn(attempt)
        if !cont || err == nil {
            break
        }
        attempt++
        if attempt > MaxRetries {
            return errMaxRetriesReached
        }
    }
    return err
}
```
Full code at https://github.com/matryer/try

Retrying 5 times
```go
var value string
err := Try(func(attempt int) (bool, err) {
    var err error
    value, err = SomeFunction()
    return attempt < 5, err //try 5 times
})
if err != nil {
    log.Fatalln("error:", err)
}
```
* Return whether to retry or not, or an error
* Easy to read

Retrying with a delay between retries
```go
var value string
err := Try(func(attempt int) (bool, error) {
    var err error
    value, err = SomeFunction()
    if err != nil {
        time.Sleep(1 * time.Minute) // wait a minute
    }
    return attempt < 5, err
})
if err != nil {
    log.Fatalln("error:", err)
}
```
* Don't bloat the Try function with arguments
* Let people do extra things with Go code where possible

## Empty struct implementations
```go
type Codec interface {
    Encode(w io.Writer, v interface{}) error
    Decode(r io.Reader, v interface{}) error
}

type jsonCodec struct{}
func(jsonCodec) Encode(w io.Writer, v interface{}) error {
    return json.NewEncoder(w).Encode(v)
}
func (jsonCodec) Decode(r io.Reader, v interface{}) error {
    return json.NewDecoder(r).Decode(v)
}

// maps the Codec interface and jsonCodec{}
var JSON Codec = jsonCodec{}
```
* Empty struct{} to group methods together
* Methods don't capture the receiver
* JSON variable doesn't expose the jsonCodec type
* Very simple API



## Semaphores
Limit the number of goroutines running at once.
```go
var (
    concurrent = 5
    semaphoreChan = make(chan struct, concurrent)
)

func doWork(item int) {
    semaphoreChan <- struct{}{} // block while full
    go func() {
        defer func() {
            <- semaphoreChan // read to release a slot
        }()
        log.Println(item)
        time.Sleep(1 * time.Second)
    }()
}

func main() {
    for i := 0; i < 10000; i++ {
        doWork(i)
    }
}
```
* Useful for concurrent anything like ssh sessions


## Be obvious not clever
```go
func something() {
    defer StartTimer("something")()
    // :-|
}
```
should be
```go
func something() {
    stop := StartTimer("something")
    defer stop()
    // :-)
}
```

# How to become a native speaker in go
* Read the standard library
* Write obvious code(not clever)
* Don't surprise your users
* Seek simplicity

Learn from others:
* Participate in open-source projects
* Ask for reviews and accept criticisms
* Help others when you spot something (and be kind)

##