# About

first class functions behavior.

### Encapsulating behavior with function values
When we invoke a function or a method, we do so passing around data. The job of the function is often to intrepret the input data and take some action. Function values allow you to pass behavior to be executed rather than data  to be intrepreted. In effect, passing a function value allows you to declare code that will execute later, perhaps in a different context.

The below example creates a calculator that knows how to add, subtract, and multiply. If we wanted to add divide we'd allocate an operation constant, and update Do method with the code to implement division. This is reasonable, but each time we need to add an operation, the Do method would grow larger and become hard to follow because each time we add an operation we have to encode into Do knowledge of how to intrepret that opertion.
```go
type Calculator struct {
    acc float64
}

const (
    OP_ADD = 1 << iota
    OP_SUB
    OP_MUL
)

func (c *Calculator) Do(op int, v float64) float64 {
    switch op {
        case OP_ADD:
            c.acc += v
        case OP_SUB:
            c.acc -= v
        case OP_MUL:
            c.acc *= v
        default:
            panic("unhandled operation")
    }
    return c.acc
}


func main() {
    var c Calculator
    fmt.Println(c.Do(OP_ADD, 100))
    fmt.Println(c.Do(OP_SUB, 50))
    fmt.Println(c.Do(OP_MUL, 2))
}

```

In the following example we have a Calculator that manages its own accumulator. The calculator has a Do method which this time takes a function as the operation, and a value as the operand. Whenever Do is called, it calls the operation we pass in using its own accumulator and the operand we provide.
```go

type Calculator struct {
    acc float64
}

type opfunc func(float64, float64) float64

func (c *Calculator) Do(op func(float64) float64) float64{
    c.acc = op(c.acc)
    return c.acc
}

func Add(n float64) func(float64) float64 {
    return func(acc float64) {
        return acc + n
    }
}

func Sub(n float64) func(float64) float64 {
    return func(acc float64) float64 {
        return acc - n
    }
}

func Mul(n float64) func(float64) float64 {
    return func(acc float64) float64 {
        return acc * n
    }
}

func Sqrt() func(float64) float64 {
    return func(n float64) float64 {
        return math.Sqrt(n)
    }
}

func main() {
    var c Calculator
    c.Do(Add(2))  // 2
    c.Do(Sqrt()) // 1.41421356237

    // function signature of our Sqrt function is the same as math.Sqrt, so
    // we can reduce needed code by reusing any function from the math package
    // that takes a single argument.
    c.Do(math.Sqrt)
    c.Do(math.Cos)
}
```
We started with a model of hard coded, interpreted logic. We moved to a more functional model, where we pass in the behaviour we want. Then, by taking it a step further, we generalised our calculator to work for operations regardless of their number of arguments.



## References
https://dave.cheney.net/2016/11/13/do-not-fear-first-class-functions
https://commandcenter.blogspot.com/2014/01/self-referential-functions-and-design.html