# aws-go functionality


#### nil attributes
The `aws.StringValue` functions are used to convert pointer types from the SDK to values. Use this to avoid dereferencing nil values.
```go
    for _, item := range instances {
    	writer.Write(&Instance{
    		InstanceId: *item.InstanceId,
			PrivateIpAddress: *item.PrivateIpAddress,
			PublicIpAddress: aws.StringValue(item.PublicIpAddress),
		})
	}
```

* https://stackoverflow.com/questions/35196823/aws-sdk-go-describestacks-trying-to-get-a-list-of-stackids-or-get-stackid-from